re:nudibranch
(remake of original project "nudibranch")


re:nudibranch is an installation art piece, meant to explore the relationship
between a subject's identity, and the ideas placed upon them by others. The
original concept of the piece was that of a "mean-spirited game," allowing
users to be entertained at the expense of another.

The subject sits in a chair, with a projecter projecting the piece directly at
their face as the words circle their head around them. Users are invited to type 
words into the text box which appears uppon the chest of the subject, adding 
them to the list surrounding the subject's head. As the subject is unable to see 
the words added to the list, users are encouraged to say whatever they like,
including things that would otherwise be hurtful.

Once hurtful or mean-spirited words begin appearing, passive vieweres and other
users may be influenced by the words they read, subconsiously influencing their
view of the subject (while also increasing the liklihood thet they themselves
will write cruel things.)

This version is a remake of the version originally presented. The project was 
entirely rewritten in Flash Develop (the original was a timeline-based Adobe IDE 
project.)

Copyright Jesse Anderson, 2011-2014.